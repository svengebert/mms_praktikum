#ifndef _MMSV2021_H_
#define _MMSV2021_H_

#define __READ_SIGNAL_BLOCK_SIZE__ 50
#define __EXTREMABLOCKSIZE__ 50

typedef struct {
  int iNumLocalExtrema;
  int *piLocalExtremaPos;
} LocalExtrema;

typedef struct {
  double *pdValues;
  int    iNumValues;
  int    iGlobalMinPos;
  int    iGlobalMaxPos;
  double dMean;
  double dVariance;
  double dMedian;
  double dArea;
  LocalExtrema *pleMinima;
  LocalExtrema *pleMaxima;
} MMSSignal;

typedef struct {
  int iNumBins;
  int *piBins;
  double dMin;
  double dMax;
} MMSHistogram;

//Aufgabe 01
double *generateSineArray(double dAmplitude, int iNumValues, int iNumSamplesPerPeriod);
MMSSignal *generateSignalSine(double dAmplitude, int iNumValues, int iNumSamplesPerPeriod);
void writeArrayToFile(char *pcFilename, double *pdValues, int iNumValues);
void writeSignalToFile(char *pcFilename, MMSSignal *pmmsOut); 
/*********************
*
* Create a MMSSignal struct with all parameters initialized
* 
* iNumValues - Number of Samples in the signal
* pdValues - Initialized array with all samples of the signal
*
* return pointer on MMSSignal struct
*
**********************/
MMSSignal *initSignal(int iNumValues, double *pdValues);
MMSSignal *readSignalFromFile(char *pcFilename);

//Aufgabe 02
void initSignalFeatures(MMSSignal *pmmsIn);
int getGlobalMaxPos(int iNumValues, double *pdValues);
int getGlobalMinPos(int iNumValues, double *pdValues);
void initLocalExtrema(MMSSignal *pmmsIn);
void initMean(MMSSignal *pmmsIn);
void initVariance(MMSSignal *pmmsIn);
void initMedian(MMSSignal *pmmsIn);
void initArea(MMSSignal *pmmsIn);
/*********************
*  
*  Create a histogram struct where all parameters are ininitialized
*  including the array of the bin counter with each bin counter set to 0
*  Bin width is implicitly given by (dMax-dMin)/iNumBins
*
*  iNumBins - Number of Bins
*  dMin - Minimal value indicating the lower limit of the first bin
*  dMax - Maximal value indicating the upper limit of the last bin
*
*  return pointer on historgram struct  
*
**********************/  
MMSHistogram *initHistogram(int iNumBins, double dMin, double dMax);

void fillHistogramFromArray(double *pdValues, int iValues, MMSHistogram *pmmhIn);
MMSHistogram *getHistogramFromSignal(MMSSignal *pmmsIn, int iNumBins);
double getEntropy(MMSHistogram *pmmhIn);


//Aufgabe 03
//Not factorial but Pascal Triangle
double *generateLineFromPascalTriangle(int iLineNumber);
MMSSignal *generateSignalLineFromPascalTriangle(int iLineNumber);

double *convoluteArrays(double *piA, int iALength, double *piB, int iBLength);
MMSSignal *convoluteSignals(MMSSignal *pmmsA, MMSSignal *pmmsB);

//Aufgabe 04
/************************************
* input 
* iNumValues - #Number of signal values
* pdReIn,pdImIn - cartesian representation of the input values
* pdReOut,pdImOut - cartesian representation of th output values
* iDir - -1 indicates forward transformation (time => frequency, generate spectrum from signal), 
*        1 indicates backward transformation (frequency => time, generate signal from spectrum)
************************************/
void computeDFT(int iNumValues, double *pdReIn, double *pdImIn, double *pdReOut, double *pdImOut, int iDir);

/**********
* cart2polar: required to compute spectrum (Amplitude,Frequency) from cartesian representation
***********/
void cart2polar(int iNumValues, double *pdReIn, double *pdImIn, double *pdAmpOut, double *pdFreqOut);

/***********
*  polar2cart: required to compute cartesian representation for DFT (Re, Im) from polar representation
************/
void polar2cart(int iNumValues, double *pdAmpIn, double *pdFreqIn, double *pdReOut, double *pdImOut);
#endif
