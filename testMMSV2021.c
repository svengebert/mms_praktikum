#include "libMMSV2021.c"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {
    printf("\n--- MMSV2021 (Gebert & Breunig) ---\n\n");    

    /*
     * TEST 1
     */
    printf("\n - TEST 1 -\n\n");
    double *pdArray = generateSineArray(4.0, 400, 50);
    writeArrayToFile("test1.txt", pdArray, 400);

    /*
     * TEST 2
     */
    printf("\n - TEST 2 -\n\n");
    MMSSignal *pmmsNewSignal = generateSignalSine(4.0, 400, 50);
    writeSignalToFile("test2.txt", pmmsNewSignal);

    /*
     * TEST 3
     */
    printf("\n - TEST 3 -\n\n");
    MMSSignal *pmmsAnotherSignal = readSignalFromFile("test2.txt");
    writeSignalToFile("test3.txt", pmmsAnotherSignal);

    /*
     * TEST 4
     */
    printf("\n - TEST 4 -\n\n");
    MMSSignal *pmmsSignal4 = readSignalFromFile("EKGsignal.txt");
    initSignalFeatures(pmmsSignal4);
    MMSHistogram *pmmhHistogram = getHistogramFromSignal(pmmsSignal4, 2);
    printSignal(pmmsSignal4, pmmhHistogram);

    /*
     * TEST 4.5
     */
    int iNumSamples = 20;

    printf("\n - TEST 4.5 -\n\n");
    MMSSignal *pmmsSignal2 = generateSignalLineFromPascalTriangle(iNumSamples);
    writeSignalToFile("test45.txt", pmmsSignal2);

    MMSSignal *pmmsSignal3 = readSignalFromFile("EKGsignal.txt");

    MMSSignal *pmmsSignal45 = convoluteSignals(pmmsSignal3, pmmsSignal2);
    
    writeSignalToFile("test451.txt", pmmsSignal45);

    /*
     * TEST 5
     */
    printf("\n - TEST 5 -\n\n");

    double *pdArray2 = generateSineArray(5.0, 20, 10);
    writeArrayToFile("test4.txt", pdArray2, 20);
    MMSSignal *pmmsSignal5 = readSignalFromFile("EKGsignal.txt");
    initSignalFeatures(pmmsSignal5);
    MMSHistogram *pmmhHistogram2 = getHistogramFromSignal(pmmsSignal5, 10);
    printSignal(pmmsSignal5, pmmhHistogram2);

    double *pdImIn = malloc(sizeof(double) * pmmsSignal5->iNumValues);
    if(pdImIn == NULL) {
        printf("Memory not allocated. (TEST, pdReResult)\n\n");
        exit(0);
    };

    for(int i = 0; i < pmmsSignal5->iNumValues; i++) {
        pdImIn[i] = 0.0;
    }

    double *pdReResult = malloc(sizeof(double) * pmmsSignal5->iNumValues);
    if(pdReResult == NULL) {
        printf("Memory not allocated. (TEST, pdReResult)\n\n");
        exit(0);
    };

    double *pdImResult = malloc(sizeof(double) * pmmsSignal5->iNumValues);
    if(pdImResult == NULL) {
        printf("Memory not allocated. (TEST, pdImResult)\n\n");
        exit(0);
    };

    double *pdAmp = malloc(sizeof(double) * pmmsSignal5->iNumValues);
    if(pdAmp == NULL) {
        printf("Memory not allocated. (TEST, pdAmp)\n\n");
        exit(0);
    };

    double *pdFreq = malloc(sizeof(double) * pmmsSignal5->iNumValues);
    if(pdFreq == NULL) {
        printf("Memory not allocated. (TEST, pdFreq)\n\n");
        exit(0);
    };

    double *pdReSignal = malloc(sizeof(double) * pmmsSignal5->iNumValues);
    if(pdReSignal == NULL) {
        printf("Memory not allocated. (TEST, pdReSignal)\n\n");
        exit(0);
    };

    double *pdImSignal = malloc(sizeof(double) * pmmsSignal5->iNumValues);
    if(pdImSignal == NULL) {
        printf("Memory not allocated. (TEST, pdImSignal)\n\n");
        exit(0);
    };

    // printf("- Signal - \n\n");
    // printf("Re:\n");
    // printDoubleArray(pmmsSignal5->pdValues, pmmsSignal5->iNumValues);
    // printf("Im:\n");
    // printDoubleArray(pdImIn, pmmsSignal5->iNumValues);

    // foward transformation
    computeDFT(pmmsSignal5->iNumValues, pmmsSignal5->pdValues, pdImIn, pdReResult, pdImResult, -1);

    // printf("- Cartesian - \n\n");
    // printf("Re:\n");
    // printDoubleArray(pdReResult, pmmsSignal5->iNumValues);
    // printf("Im:\n");
    // printDoubleArray(pdImResult, pmmsSignal5->iNumValues);

    // cart2polar needed
    cart2polar(pmmsSignal5->iNumValues, pdReResult, pdImResult, pdAmp, pdFreq);

    // fix Amplitude? x2 / iNumSamples
    // for(int i = 0; i < pmmsSignal5->iNumValues; i++) {
    //     pdAmp[i] = (2 * pdAmp[i]) / pmmsSignal5->iNumValues;
    // }

    // printf("- Spectrum (Polar) - \n\n");
    // printf("Amp:\n");
    // printDoubleArray(pdAmp, pmmsSignal5->iNumValues);
    // printf("Freq:\n");
    // printDoubleArray(pdFreq, pmmsSignal5->iNumValues);

    // TEST changes
    writeArrayToFile("freqs.txt", pdAmp, pmmsSignal5->iNumValues);

    polar2cart(pmmsSignal5->iNumValues, pdAmp, pdFreq, pdReResult, pdImResult);

    // printf("- Cartesian - \n\n");
    // printf("Re:\n");
    // printDoubleArray(pdReResult, pmmsSignal5->iNumValues);
    // printf("Im:\n");
    // printDoubleArray(pdImResult, pmmsSignal5->iNumValues);

    computeDFT(pmmsSignal5->iNumValues, pdReResult, pdImResult, pdReSignal, pdImSignal, 1);

    // printf("- New Signal - \n\n");
    // printf("Re:\n");
    // printDoubleArray(pdReSignal, pmmsSignal5->iNumValues);
    // printf("Im:\n");
    // printDoubleArray(pdImSignal, pmmsSignal5->iNumValues);

    double *pdNewSignalValues = malloc(sizeof(double) * pmmsSignal5->iNumValues);
    if(pdNewSignalValues == NULL) {
        printf("Memory not allocated. (TEST, pdNewSignalValues)\n\n");
        exit(0);
    };

    for(int i = 0; i < pmmsSignal5->iNumValues; i++) {
        pdNewSignalValues[i] = pdReSignal[i] + pdImSignal[i];
    }

    writeArrayToFile("testDFT.txt", pdReSignal, pmmsSignal5->iNumValues);

    return 0;
};