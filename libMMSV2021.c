#include "MMSignal.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

/*
 * Notes
 * 
 * Mail: Betreff: MMS WS20/21, Inhalt: Namen
 * To plot single column graph use -> plot "test1.txt" using ($0/1.):1 w linesp
 */

/*
 * Declarations needed ...
 */
void printDoubleArray(double *pdArray, int iNumValues);
void printIntegerArray(int *piArray, int iNumValues);
MMSSignal *initSignal(int iNumValues, double *pdValues);
int compare(const void *a, const void *b);

/*
 * MAIN LIBRARY
 */

/*
 * AUFGABE 1
 */

/* 
 * FUNCTION: GENERATE SINE ARRAY
 * 
 * Generate sequence of double values representing samples of a sine wave
 *
 * dAmplitude - Maximum value of sine wave
 * iNumValues - Length of Sequence
 * iNumSamplesPerPeriod - Frequency
 *
 * Return Value - Pointer on array containing the sequence 
 */
double *generateSineArray(double dAmplitude, int iNumValues, int iNumSamplesPerPeriod) {

    // Allocate memory for values
    double *pdSine = malloc(iNumValues * sizeof(double));
    if(pdSine == NULL) {
        printf("Memory not allocated. (generateSineArray, pdSine)\n\n");
        exit(0);
    }

    /*
     * Generate sine wave: y(t) = A * sin(2 * pi * f * t)
     * 
     * A = pAmplitude
     * pi = M_PI = 3.14
     * f = 1 / iNumSamplesPerPeriod
     * t = i % iNumSamplesPerPeriod OR 1 + i * f
     */
    for(int i = 0; i < iNumValues; i++)
        // pdSine[i] = dAmplitude * sin(((2 * M_PI) / iNumSamplesPerPeriod) * (i * (1 + 1 / iNumSamplesPerPeriod)));
        pdSine[i] = dAmplitude * sin(((2 * M_PI) / iNumSamplesPerPeriod) * (i % iNumSamplesPerPeriod));

    // printDoubleArray(pdSine, iNumValues);
    printf("SineArray generated. (generateSineArray)\n\n");
    return pdSine;
}

/*
 * FUNCTION: GENERATE SIGNAL SINE
 * 
 * Generate MMSSignal (iNumsValues and pdValues set) and others initialized with 0
 * 
 * Use: initSignal(), generateSineArray()
 * 
 * dAmplitude - Maximum Value of sine wave
 * iNumValues - Length of Sequence
 * iNumSamplesPerPeriod - Frequency
 * 
 * Return Value - Pointer on MMSSignal containing fully initialized sine signal
 */
MMSSignal *generateSignalSine(double dAmplitude, int iNumValues, int iNumSamplesPerPeriod) {

    double *pdValues = generateSineArray(dAmplitude, iNumValues, iNumSamplesPerPeriod);

    MMSSignal *pmmsSignal = initSignal(iNumValues, pdValues);

    printf("Sine signal generated. (generateSignalSine)\n\n");

    return pmmsSignal;
}

/*
 * FUNCTION: WRITE ARRAY TO FILE
 * 
 * Write array to file (single column)
 * 
 * pcFilename - Name of file
 * pdValues - Pointer to array which is written to file
 * iNumValues - Number of values within the array
 */
void writeArrayToFile(char *pcFilename, double *pdValues, int iNumValues) {
    
    FILE *pfOut;

    pfOut = fopen(pcFilename, "w");
    if(pfOut == NULL) {
        printf("File not found. (writeArraytoFile, pfOut)\n\n");
        exit(0);
    }

    // write pdValues to file 'pcFilename'
    for(int i = 0; i < iNumValues; i++)
        fprintf(pfOut, "%lf\n", pdValues[i]);

    printf("Array written to file. (writeArraytoFile)\n\n");

    fclose(pfOut);
}

/*
 * FUNCTION: WRITE SIGNAL TO FILE
 * 
 * Write values of a signal to file (single column)
 * 
 * Use: writeArraytoFile()
 * 
 * pcFilename - Name of file
 * pmmsOut - MMSSignal struct containing the array which is written to file
 */
void writeSignalToFile(char *pcFilename, MMSSignal *pmmsOut) {

    writeArrayToFile(pcFilename, pmmsOut->pdValues, pmmsOut->iNumValues);

    printf("Signal written to file. (writeSignaltoFile)\n\n");
}

/*
 * FUNCTION: INIT SIGNAL
 * 
 * Generate MMSSignal struct with all parameters initialized
 * 
 * iNumValues - Number of samples in the signal
 * pdValues - Initialized array with all samples of the signal
 * 
 * Return Value - Pointer on initialized MMSSignal
 */
MMSSignal *initSignal(int iNumValues, double *pdValues) {

    MMSSignal *pmmsSignal = malloc(sizeof(MMSSignal));
    if(pmmsSignal == NULL) {
        printf("Memory not allocated. (initSignal, pmmsSignal)\n\n");
        exit(0);
    }

    LocalExtrema *pleMinima = malloc(sizeof(LocalExtrema));
    if(pleMinima == NULL) {
        printf("Memory not allocated. (initSignal, pleMinima)\n\n");
        exit(0);
    }

    LocalExtrema *pleMaxima = malloc(sizeof(LocalExtrema));
    if(pleMaxima == NULL) {
        printf("Memory not allocated. (initSignal, pleMaxima)\n\n");
        exit(0);
    }

    pmmsSignal->iNumValues = iNumValues;
    pmmsSignal->pdValues = pdValues;
    pmmsSignal->iGlobalMinPos = 0;
    pmmsSignal->iGlobalMaxPos = 0;
    pmmsSignal->dMean = 0;
    pmmsSignal->dVariance = 0;
    pmmsSignal->dMedian = 0;
    pmmsSignal->dArea = 0;

    pmmsSignal->pleMinima = pleMinima;
    pmmsSignal->pleMaxima = pleMaxima;

    pmmsSignal->pleMinima->iNumLocalExtrema = 0;
    pmmsSignal->pleMinima->iNumLocalExtrema = 0;

    printf("Signal initialized. (initSignal)\n\n");
    return pmmsSignal;
}

/*
 * FUNCTION: READ SIGNAL FROM FILE
 * 
 * Read signal values from file and generate MMSSignal struct with all parameters initialized
 * 
 * Use: initSignal()
 * 
 * pcFilename - Name of file containing signal values
 * 
 * Return Value - Pointer on initialized MMSSignal
 */
MMSSignal *readSignalFromFile(char *pcFilename) {

    int n = 0;
    int iBlocks = 1;
    double dInput = 0;
    double *pdDynArray = NULL;
    MMSSignal *pmmsSignal;

    // allocate memory for dynArray
    pdDynArray = calloc(__READ_SIGNAL_BLOCK_SIZE__, sizeof(double));
    if(pdDynArray == NULL) {
        printf("Memory not allocated. (readSignalFromFile, pdDynArray)\n\n");
        exit(0);
    }

    FILE *pfIn = fopen(pcFilename, "r");
    if(pfIn == NULL) {
        printf("File not found. (readSignalFromFile, pfIn)\n\n");
        exit(0);
    }

    /*  read until EOF reached - reallocate memory if needed [blocks of 50] -
        quit if unexpected character was found */
    while(1) {

        int r = fscanf(pfIn, "%lf", &dInput);
        if(r > 0) {
            if(n >= iBlocks * __READ_SIGNAL_BLOCK_SIZE__) {
                iBlocks++;
                pdDynArray = realloc(pdDynArray, iBlocks * __READ_SIGNAL_BLOCK_SIZE__ * sizeof(double));
                if(pdDynArray == NULL) {
                    printf("Memory not reallocated. (readSignalFromFile, pdDynArray)\n\n");
                    exit(0);
                }
            }
            pdDynArray[n] = dInput;
            n++;

        } else if(r == EOF) {

            pmmsSignal = initSignal(n, pdDynArray);

            printf("Signal read from file '%s'. (readSignalFromFile)\n\n", pcFilename);
            break;

        } else {
            printf("Unexpected character. (readSignalFromFile)\n\n");
            exit(0);
        }
    }

    fclose(pfIn);

    return pmmsSignal;
}

/*
 * AUFGABE 2
 */

/*
 * FUNCTION: INIT SIGNAL FEATURES
 * 
 * Compute all signal features (extrema, mean, variance, median, area, entropy)
 * 
 * pmmsIn - Pointer on MMSSignal struct which is computed
 */
void initSignalFeatures(MMSSignal *pmmsIn) {

    pmmsIn->iGlobalMinPos = getGlobalMinPos(pmmsIn->iNumValues, pmmsIn->pdValues);
    pmmsIn->iGlobalMaxPos = getGlobalMaxPos(pmmsIn->iNumValues, pmmsIn->pdValues);

    initLocalExtrema(pmmsIn);

    initMean(pmmsIn);
    initVariance(pmmsIn);
    initMedian(pmmsIn);
    initArea(pmmsIn);
}

/*
 * FUNCTION: GET GLOBAL MAX POS
 * 
 * Find position of global maximum
 * 
 * iNumValues - Number of samples in the signal
 * pdValues - Initialized array with all samples of the signal
 * 
 * Return Value - Position of global maximum
 */
int getGlobalMaxPos(int iNumValues, double *pdValues) {

    double dCurrentMax = pdValues[0];
    int iGlobalMaxPos = 0;
    bool bIsUndefined = false;

    for(int i = 0; i < iNumValues; i++) {

        if(pdValues[i] > dCurrentMax) {
            dCurrentMax = pdValues[i];
            iGlobalMaxPos = i;
        }
    }

    if(pdValues[iGlobalMaxPos] == pdValues[iGlobalMaxPos+1])
        printf("This signal contains an undefined global maximum. (getGlobalMaxPos)\n\n");

    return iGlobalMaxPos;
}

/*
 * FUNCTION: GET GLOBAL MIN POS
 * 
 * Find position of global minimum
 * 
 * iNumValues - Number of samples in the signal
 * pdValues - Initialized array with all samples of the signal
 * 
 * Return Value - Position of global minimum
 */
int getGlobalMinPos(int iNumValues, double *pdValues) {

    double dCurrentMin = pdValues[0];
    int iGlobalMinPos = 0;
    bool bIsUndefined = false;

    for(int i = 0; i < iNumValues; i++) {

        if(pdValues[i] < dCurrentMin) {
            dCurrentMin = pdValues[i];
            iGlobalMinPos = i;
        }
    }

    if(pdValues[iGlobalMinPos] == pdValues[iGlobalMinPos+1])
        printf("This signal contains an undefined global minimum. (getGlobalMinPos)\n\n");

    return iGlobalMinPos;
}

/*
 * FUNCTION: INIT LOCAL EXTREMA
 * 
 * Initialize local extrema of MMSSignal struct
 * (set iNumLocalExtrema and piLocalExtremaPos of both minima and maxima)
 * 
 * pmmsIn - Pointer on MMSSignal struct which is computed
 */
void initLocalExtrema(MMSSignal *pmmsIn) {

    int iMinimaBlocks = 1;
    int iMaximaBlocks = 1;
    int iNumLocalMinima = 0;
    int iNumLocalMaxima = 0;
    bool bIsUndefined = false;

    double *pdVs = pmmsIn->pdValues;

    // Minima
    LocalExtrema *pleMinima = malloc(sizeof(LocalExtrema));
    if(pleMinima == NULL) {
        printf("Memory not allocated. (initLocalExtrema, pleMinima)\n\n");
        exit(0);
    }

    int *iLocalMinimaPos = calloc(__EXTREMABLOCKSIZE__, sizeof(int));
    if(iLocalMinimaPos == NULL) {
        printf("Memory not allocated. (initLocalExtrema, iLocalMinimaPos)\n\n");
        exit(0);
    }

    // Maxima
    LocalExtrema *pleMaxima = malloc(sizeof(LocalExtrema));
    if(pleMaxima == NULL) {
        printf("Memory not allocated. (initLocalExtrema, pleMaxima)\n\n");
        exit(0);
    }

    int *iLocalMaximaPos = calloc(__EXTREMABLOCKSIZE__, sizeof(int));
    if(iLocalMaximaPos == NULL) {
        printf("Memory not allocated. (initLocalExtrema, iLocalMaximaPos)\n\n");
        exit(0);
    }

    // check pdValues for local minima and maxima and save positions to arrays
    for(int i = 1; i < pmmsIn->iNumValues - 1; i++) {
        
        // extend/realloc array if needed
        if(iNumLocalMinima >= iMinimaBlocks * __EXTREMABLOCKSIZE__) {
            iMinimaBlocks++;
            iLocalMinimaPos = realloc(iLocalMinimaPos, iMinimaBlocks * __EXTREMABLOCKSIZE__ * sizeof(int));
            if(iLocalMinimaPos == NULL) {
                printf("Memory not reallocated. (initLocalExtrema, iLocalMinimaPos)\n\n");
                exit(0);
            }
        }
        if(iNumLocalMaxima >= iMaximaBlocks * __EXTREMABLOCKSIZE__) {
            iMaximaBlocks++;
            iLocalMaximaPos = realloc(iLocalMaximaPos, iMaximaBlocks * __EXTREMABLOCKSIZE__ * sizeof(int));
            if(iLocalMaximaPos == NULL) {
                printf("Memory not reallocated. (initLocalExtrema, iLocalMaximaPos)\n\n");
                exit(0);
            }
        }

        // greater than predecessor?
        if(pdVs[i] > pdVs[i-1]) {
            // greater than successor?
            if(pdVs[i] > pdVs[i+1]) {
                // MAXIMA
                iLocalMaximaPos[iNumLocalMaxima] = i;
                iNumLocalMaxima++;
            }
            // same as successor?
            else if (pdVs[i] == pdVs[i+1]) {
                int k = 1;
                if(i < (pmmsIn->iNumValues - 2)) {
                    while(pdVs[i] == pdVs[i+1*k]) {
                        k++;
                        if(i >= (pmmsIn->iNumValues - k)) {
                            break;
                        }
                    }
                    if(pdVs[i] > pdVs[i+k]) {
                        // undefined MAXIMA
                        iLocalMaximaPos[iNumLocalMaxima] = i + (k/2);
                        iNumLocalMaxima++;
                        bIsUndefined = true;
                        i++;
                    }
                }
            }
        }
        // less than predecessor?
        else if(pdVs[i] < pdVs[i-1]) {
            // less than successor?
            if(pdVs[i] < pdVs[i+1]) {
                // MINIMA
                iLocalMinimaPos[iNumLocalMinima] = i;
                iNumLocalMinima++;
            }
            // same as successor?
            else if (pdVs[i] == pdVs[i+1]) {
                int k = 1;
                if(i < (pmmsIn->iNumValues - 2)) {
                    while(pdVs[i] == pdVs[i+1*k]) {
                        k++;
                        if(i >= (pmmsIn->iNumValues - k)) {
                            break;
                        }
                    }
                    if(pdVs[i] < pdVs[i+k]) {
                        // undefined MAXIMA
                        iLocalMinimaPos[iNumLocalMinima] = i + (k/2);
                        iNumLocalMinima++;
                        bIsUndefined = true;
                        i++;
                    }
                }
            }
        }
    }

    if(bIsUndefined)
        printf("This signal contains undefined local extrema. (initLocalExtrema)\n\n");

    pleMinima->iNumLocalExtrema = iNumLocalMinima;
    pleMinima->piLocalExtremaPos = iLocalMinimaPos;

    pleMaxima->iNumLocalExtrema = iNumLocalMaxima;
    pleMaxima->piLocalExtremaPos = iLocalMaximaPos;

    pmmsIn->pleMinima = pleMinima;
    pmmsIn->pleMaxima = pleMaxima;
}

/*
 * FUNCTION: INIT MEAN
 * 
 * Initialize mean value of MMSSignal struct
 * 
 * pmmsIn - Pointer on MMSSignal struct which is computed
 */
void initMean(MMSSignal *pmmsIn) {
    double dSum = 0;

    for(int i = 0; i < pmmsIn->iNumValues; i++)
        dSum += pmmsIn->pdValues[i];

    if(pmmsIn->iNumValues == 0)
        pmmsIn->dMean = 0;
    else
        pmmsIn->dMean = dSum / pmmsIn->iNumValues;
}

/*
 * FUNCTION: INIT VARIANCE
 * 
 * Initialize variance of MMSSignal struct
 * 
 * pmmsIn - Pointer on MMSSignal struct which is computed
 */
void initVariance(MMSSignal *pmmsIn) {

    double dSum = 0;

    for(int i = 0; i < pmmsIn->iNumValues; i++)
        dSum += pow((pmmsIn->pdValues[i] - pmmsIn->dMean), 2);

    pmmsIn->dVariance = dSum / pmmsIn->iNumValues;
}

/*
 * FUNCTION: INIT MEDIAN
 * 
 * Initialize merdian of MMSSignal struct
 * 
 * pmmsIn - Pointer on MMSSignal struct which is computed
 */
void initMedian(MMSSignal *pmmsIn) {

    double dMedian = 0;
    double dTmpArray[pmmsIn->iNumValues];

    for(int i = 0; i < pmmsIn->iNumValues; i++)
        dTmpArray[i] = pmmsIn->pdValues[i];

    qsort(dTmpArray, pmmsIn->iNumValues, sizeof(double), compare);

    // uneven number of samples
    if(pmmsIn->iNumValues % 2)
        pmmsIn->dMedian = dTmpArray[pmmsIn->iNumValues /  2];
    // even number of samples
    else
        pmmsIn->dMedian = (dTmpArray[pmmsIn->iNumValues /  2] + dTmpArray[(pmmsIn->iNumValues /  2) - 1]) / 2;
}

/*
 * FUNCTION: INIT AREA
 * 
 * Initialize area of MMSSignal struct
 * 
 * pmmsIn - Pointer on MMSSignal struct which is computed
 */
void initArea(MMSSignal *pmmsIn) {

    double dSum = 0;

    for(int i = 0; i < pmmsIn->iNumValues; i++)
        dSum += pmmsIn->pdValues[i];

    pmmsIn->dArea = dSum;
}

/*********************
*  
*  Create a histogram struct where all parameters are ininitialized
*  including the array of the bin counter with each bin counter set to 0
*  Bin width is implicitly given by (dMax-dMin)/iNumBins
*
*  iNumBins - Number of Bins
*  dMin - Minimal value indicating the lower limit of the first bin
*  dMax - Maximal value indicating the upper limit of the last bin
*
*  return pointer on historgram struct  
*
**********************/  
MMSHistogram *initHistogram(int iNumBins, double dMin, double dMax) {

    MMSHistogram *pmmhHistogram = malloc(sizeof(MMSHistogram));
    if(pmmhHistogram == NULL) {
        printf("Memory not allocated. (initHistogram, pmmhHistogram)\n\n");
        exit(0);
    }

    int *piBins = malloc(iNumBins * sizeof(int));
    if(piBins == NULL) {
        printf("Memory not allocated. (initHistogram, piBins)\n\n");
        exit(0);
    }

    for(int i = 0; i < iNumBins; i++)
        piBins[i] = 0;

    pmmhHistogram->iNumBins = iNumBins;
    pmmhHistogram->piBins = piBins;
    pmmhHistogram->dMin = dMin;
    pmmhHistogram->dMax = dMax;

    return pmmhHistogram;
}

/*
 * FUNCTION: FILL HISTOGRAM FROM ARRAY
 * 
 * Fill historgram with values of an array
 * 
 * pdValues - Initialized array with all samples of a possible signal
 * iValues - Number of samples in the array
 * pmmhIn - Pointer on MMSHistogram struct which is computed
 */
void fillHistogramFromArray(double *pdValues, int iValues, MMSHistogram *pmmhIn) {

    double dTmpArray[iValues];

    for(int i = 0; i < iValues; i++)
        dTmpArray[i] = pdValues[i];

    qsort(dTmpArray, iValues, sizeof(double), compare);

    double dMin = pmmhIn->dMin;
    double dMax = pmmhIn->dMax;
    double iNumBins = pmmhIn->iNumBins;
    double dRange = 0;

    // calculate range of each bin
    if(dMin < 0 && dMax < 0)
        dRange = ((-1) * dMin + (-1) * dMax) / (double)iNumBins;
    else if(dMin < 0 && dMax >= 0)
        dRange = ((-1) * dMin + dMax) / (double)iNumBins;
    else if(dMin >= 0 && dMax < 0)
        dRange = (dMin + (-1) * dMax) / (double)iNumBins;
    else
        dRange = (dMin + dMax) / (double)iNumBins;

    int iBin = 1;
    for(int i = 0; i < iValues; i++) {
        if(dTmpArray[i] <= dMin + iBin * dRange) {
            pmmhIn->piBins[iBin - 1] += 1;
        } else {
            pmmhIn->piBins[iBin] += 1;
            iBin++;
        }
    }
}

/*
 * FUNCTION: GET HISTOGRAM FROM SIGNAL
 * 
 * Use initHistogram()
 * 
 * Generate MMSHistogram struct with MMSSignal as input
 * 
 * pmmsIn - Pointer on MMSSignal which is computed
 * iNumBins - Number of bins of the histogram
 * 
 * Return Value - Pointer on MMSHistogram struct
 */
MMSHistogram *getHistogramFromSignal(MMSSignal *pmmsIn, int iNumBins) {

    double dMin = pmmsIn->pdValues[pmmsIn->iGlobalMinPos];
    double dMax = pmmsIn->pdValues[pmmsIn->iGlobalMaxPos];

    MMSHistogram *pmmhHistogram = initHistogram(iNumBins, dMin, dMax);

    fillHistogramFromArray(pmmsIn->pdValues, pmmsIn->iNumValues, pmmhHistogram);

    return pmmhHistogram;
}

/*
 * FUNCTION: GET ENTROPY
 * 
 * Get entropy of histogram
 * 
 * pmmhIn - Pointer on MMSHistogram struct which is computed
 * 
 * Return Value - entropy value
 */
double getEntropy(MMSHistogram *pmmhIn) {

    double dEntropy = 0;
    int iNumSamples = 0;
    double p = 0;

    for(int i = 0; i < pmmhIn->iNumBins; i++) {
        iNumSamples += pmmhIn->piBins[i];
    }

    if(iNumSamples != 0) {
        for(int i = 0; i < pmmhIn->iNumBins; i++) {
            p = (double)pmmhIn->piBins[i] / (double)iNumSamples;
            // calculate Shannon entropy
            dEntropy += p * log2(p);
        }
    }

    // A value greater than 1 will be wrong if it exceeds log2 k (k = numOfEvents)
    // -> possible solutions: (-1) * 1 / log(iNumValues) * [SUMi] Pi * log(Pi)
    return (-1) * dEntropy;
}

/*
 * AUFGABE 3
 */

double *generateLineFromPascalTriangle(int iLineNumber) {
    
    double *result = malloc(iLineNumber * sizeof(double));
    double arr[iLineNumber][iLineNumber];
 
    for(int n = 0; n < iLineNumber; n++) {
        for(int k = 0; k <= n; k++) {
            if(k == n || k == 0){
                arr[n][k] = 1.0;
                result[k] = arr[n][k];
            } else {
                arr[n][k] = arr[n-1][k-1] + arr[n-1][k];
                result[k] = arr[n][k];
            }
        }
    } 

    return result;
}

MMSSignal *generateSignalLineFromPascalTriangle(int iLineNumber) {
    MMSSignal *pmmsOutSignal = initSignal(iLineNumber, generateLineFromPascalTriangle(iLineNumber));

    return pmmsOutSignal;
}

 double *convoluteArrays(double *piA, int iALength, double *piB, int iBLength) {
    
    int iNumSamples = iALength + iBLength;

    double *result = malloc(sizeof(double) * iNumSamples);
    int n = 1;
    int k = 0;

    for (n = 1; n < iNumSamples; n++) {
        double dSum = 0;
        for (k = 0; k <= n; k++) {
            if (!((k > iALength) || ((n - k) >= iBLength))) {
                dSum += (piA[k] * piB[n - k]);
            }
        }
        result[n] = dSum;
    }

    return result;
}

MMSSignal *convoluteSignals(MMSSignal *pmnsA,MMSSignal *pmnsB) {

    int iLength = pmnsB->iNumValues + pmnsA->iNumValues;
    MMSSignal *pmmsOutSignal = initSignal(iLength, convoluteArrays(pmnsA->pdValues, pmnsA->iNumValues, pmnsB->pdValues, pmnsB->iNumValues));

    return pmmsOutSignal;
}

/*
 * AUFGABE 4
 */

/*
 * FUNCTION: COMPUTE DFT
 * 
 * Compute Fourier-Transformation
 * 
 * iNumValues - Number of signal values
 * pdReIn,pdImIn - cartesian representation of the input values
 * pdReOut,pdImOut - cartesian representation of the output values
 * iDir -   -1 indicates forward transformation (time => frequency, generate spectrum from signal), 
 *           1 indicates backward transformation (frequency => time, generate signal from spectrum)
 */
void computeDFT(int iNumValues, double *pdReIn, double *pdImIn, double *pdReOut, double *pdImOut, int iDir) {

    double dRe = 0;
    double dIm = 0;
    double dReResult = 0;
    double dImResult = 0;
    double w = 2 * M_PI / iNumValues;

    for(int k = 0; k < iNumValues; k++) {

        for(int m = 0; m < iNumValues; m++) {
            // Re = a1*a2 - b1*b2
            dRe = pdReIn[m] * cos(w * m * k * (-1) * iDir) - pdImIn[m] * sin(w * m * k * (-1) * iDir);
            // printf("dRe: %lf\n", dRe);
            // Im = a1*b2 + b1*a2
            dIm = pdReIn[m] * sin(w * m * k * (-1) * iDir) + pdImIn[m] * cos(w * m * k * (-1) * iDir);
            // printf("dIm: %lf\n", dIm);
            dReResult += dRe;
            dImResult += dIm;
        }

        // printf("dReResult: %lf\n", dReResult);
        // printf("dImResult: %lf\n", dImResult);

        if(iDir == -1) {
            pdReOut[k] = dReResult;
            pdImOut[k] = dImResult;
        }
        else if(iDir == 1) {
            pdReOut[k] = dReResult / iNumValues;
            pdImOut[k] = dImResult / iNumValues;
        }

        dReResult = 0;
        dImResult = 0;
    }
}

/*
 * FUNCTION: CART to POLAR
 * 
 * Required to compute spectrum (Amplitude,Frequency) from cartesian representation
 * 
 * iNumValues - Number of signal values
 * pdReIn,pdImIn - Cartesian representation of the input values
 * pdAmpOut, pdFreqOut - Polar representation of the input values ("output")
 */
void cart2polar(int iNumValues, double *pdReIn, double *pdImIn, double *pdAmpOut, double *pdFreqOut) {

    double dRadius = 0;
    double dAngle = 0;

    for(int i = 0; i < iNumValues; i++) {

        // calculate radius by using pythagorean theorem
        dRadius = sqrt(pdReIn[i] * pdReIn[i] + pdImIn[i] * pdImIn[i]);

        // calculate angle
        if(pdReIn[i] == 0 && pdImIn[i] >= 0) {
            dAngle = M_PI/2;
        }
        else if(pdReIn[i] == 0 && pdImIn[i] < 0) {
            dAngle = 3 * M_PI/2;
        }
        // quadrant I
        else if(pdReIn[i] >= 0 && pdImIn[i] >= 0) {
            dAngle = atan(pdImIn[i] / pdReIn[i]);
        }
        // quadrant II
        else if(pdReIn[i] < 0 && pdImIn[i] >= 0) {
            dAngle = atan(pdImIn[i] / pdReIn[i]) + M_PI/2;
        }
        // quadrant III
        else if(pdReIn[i] < 0 && pdImIn[i] < 0) {
            dAngle = atan(pdImIn[i] / pdReIn[i]) + M_PI;
        }
        // quadrant IV
        else if(pdReIn[i] > 0 && pdImIn[i] < 0) {
            dAngle = atan(pdImIn[i] / pdReIn[i]) + 3 * M_PI/2;
        }

        pdAmpOut[i] = dRadius;
        pdFreqOut[i] = dAngle;
    }
}

/*
 * FUNCTION: POLAR to CART
 * 
 * Required to compute cartesian representation for DFT (Re, Im) from polar representation
 * 
 * iNumValues - Number of signal values
 * pdAmpIn, pdFreqIn - Polar representation of the input values
 * pdReOut,pdImOut - Cartesian representation of the input values ("output")
 */
void polar2cart(int iNumValues, double *pdAmpIn, double *pdFreqIn, double *pdReOut, double *pdImOut) {

    for(int i = 0; i < iNumValues; i++) {
        pdReOut[i] = pdAmpIn[i] * cos(pdFreqIn[i]);
        pdImOut[i] = pdAmpIn[i] * sin(pdFreqIn[i]);
    }
}

/*
 * Other functions ..
 */

/* 
 * FUNCTION: PRINT DOUBLE ARRAY
 * 
 * Prints values of double array to console
 *
 * pdArray : Pointer to double array
 * iNumValues: Length of double array
 */
void printDoubleArray(double *pdArray, int iNumValues) {

    for(int i = 0; i < iNumValues; i++)
        printf("%lf \n", pdArray[i]);
    printf("\n");
}

/* 
 * FUNCTION: PRINT INT ARRAY
 * 
 * Prints values of int array to console
 *
 * piArray : Pointer to int array
 * iNumValues: Length of int array
 */
void printIntegerArray(int *piArray, int iNumValues) {

    printf("[");
    for(int i = 0; i < iNumValues; i++) {
        if(i < iNumValues - 1)
            printf("%d, ", piArray[i]);
        else
            printf("%d]\n", piArray[i]);
    }
}

/*
 * FUNCTION: COMPARE
 * 
 * Compare two doubles (used for sort algorithm)
 *      - casting to double pointer and refer to value with *
 */
int compare(const void *a, const void *b) {

    if(*(double*) a > *(double*) b)
        return 1;
    else if(*(double*) a < *(double*) b)
        return -1;
    else
        return 0;
}

/*
 * FUNCTION: PRINT SIGNAL
 * 
 * Print MMSSignal struct
 */
void printSignal(MMSSignal *pmmsIn, MMSHistogram *pmmhIn) {

    printf("-- Print Signal --\n\n");

    printf("iNumValues: %d\n", pmmsIn->iNumValues);
    printf("iGlobalMinPos: %d\n", pmmsIn->iGlobalMinPos);
    printf("iGlobalMaxPos: %d\n", pmmsIn->iGlobalMaxPos);

    // Print pleMinima struct
    printf("pleMinima->iNumLocalExtrema: %d\n", pmmsIn->pleMinima->iNumLocalExtrema);
    if(pmmsIn->pleMinima->piLocalExtremaPos != NULL) {
        printf("pleMinima->piLocalExtremaPos: ");
        printIntegerArray(pmmsIn->pleMinima->piLocalExtremaPos, pmmsIn->pleMinima->iNumLocalExtrema);
    }

    // Print pleMaxima struct
    printf("pleMaxima->iNumLocalExtrema: %d\n", pmmsIn->pleMaxima->iNumLocalExtrema);
    if(pmmsIn->pleMaxima->piLocalExtremaPos != NULL) {
        printf("pleMaxima->piLocalExtremaPos: ");
        printIntegerArray(pmmsIn->pleMaxima->piLocalExtremaPos, pmmsIn->pleMaxima->iNumLocalExtrema);
    }

    printf("dMean: %lf\n", pmmsIn->dMean);
    printf("dVariance: %lf\n", pmmsIn->dVariance);
    printf("dMedian: %lf\n", pmmsIn->dMedian);
    printf("dArea: %lf\n", pmmsIn->dArea);

    // Print MMSHistogram struct
    if(pmmhIn != NULL) {
        printf("\n-- Print Histogram --\n\n");
        printf("iNumBins: %d\n", pmmhIn->iNumBins);
        printf("piBins: ");
        printIntegerArray(pmmhIn->piBins, pmmhIn->iNumBins);
        printf("dMin: %lf\n", pmmhIn->dMin);
        printf("dMax: %lf\n", pmmhIn->dMax);
        printf("dEntropy: %lf\n", getEntropy(pmmhIn));
    }

    printf("\n");
}